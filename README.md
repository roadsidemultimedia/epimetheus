# Epimetheus

Epimetheus was Prometheus' stupid brother. It's also a super useful WordPress theme.

## Installation

To be determined.

## Contributing


### Getting Started

After cloning the repository, get the environment up and running with the command:

    $ vagrant up

Visit http://localhost:3000 in your browser to view the WordPress installation. The theme code in the folder `./epimetheus/` will be symlinked into the `wp-content/themes/epimetheus`, so you can start editing without having to move the code into place. Be sure to enable theme, though.

**Admin Login**

Username: admin

Password: test

### Testing

Once you have your Vagrant box up and running, log into it with:

    $ vagrant ssh

You can then run any unit tests as you develop by changing directories into the development directory and running Guard, which will watch your files for changes:

    $ cd wordpress-devel && guard -p

Now you can get to work. As you edit and save files, any unit tests will be run automatically. Be sure not to commit anything if the tests are failing!