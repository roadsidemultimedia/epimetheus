<?php
class Test_Taxonomies extends WP_UnitTestCase {
    function setUp(){
        parent::setUp();
    }
    function tearDown(){
        parent::tearDown();
    }
    function test_all_taxonomies() {
        $all_tax = get_object_taxonomies( 'post' );
        sort( $all_tax );
        $this->assertEquals( array( 'category', 'channel-placement', 'post_format', 'post_tag' ), $all_tax );
    }
}
?>