#!/usr/bin/python

import os, os.path
import sys
from subprocess import Popen, PIPE
import yaml

def error_out(msg="", exit_code=1):
	stdout = sys.stderr
	print msg
	sys.exit(1)

def run_command(cmd, data=None):
	result = Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True)
	out = result.stdout.read()
	err = result.stderr.read()
	result.communicate(data)
	code = result.returncode
	return [out, err, code]

def multisite(config):
	if not config['slug']:
		error_out("Error. Slug not defined in config for %s" % fname)
	slug = config['slug']

	if not config.has_key('site_title'):
		error_out("Error. site_title not defined in config for %s" % slug)

	staging_host = "%s.%s" % (slug, staging_domain)
	site_title = config['site_title']
	vhost_path = "/var/www/app"

	# defaults for debugging
	out = ""
	err = ""
	code = 0

	if not config['enabled']:
		print "Disabling production for %s" % slug
		args = (wp_cli, vhost_path, slug)
		cmd = "%s site delete --path=%s --keep-tables --allow-root --yes --slug %s" % args
		print cmd
		out, err, code = run_command(cmd)
		if code != 0:
			error_out(err)

		return

	print "Ensuring production site exists for %s" % slug
	args = (wp_cli, vhost_path, slug, site_title, email)
	cmd = "%s site create --path=%s --allow-root --slug=%s --title='%s' --email=%s" % args
	print cmd
	out, err, code = run_command(cmd)
	if code != 0:
		error_out(err)

def singlesite(config):
	if not config['slug']:
		error_out("Error. Slug not defined in config for %s" % fname)
	slug = config['slug']

	if not config.has_key('site_title'):
		error_out("Error. site_title not defined in config for %s" % slug)

	staging_host = "%s.%s" % (slug, staging_domain)

	# defaults for debugging
	out = ""
	err = ""
	code = 0

	vhost_path = os.path.join(vhosts_dir, slug)

	if not config['enabled']:
		print "Disabling %s" % slug
		if os.path.isdir(vhost_path):
			print "Removing files at %s" % vhost_path
			run_command("rm -Rf %s" % vhost_path)

		if os.path.islink("/etc/nginx/sites-enabled/%s" % slug):
			print "Disabling virtualhost"
			os.unlink("/etc/nginx/sites-enabled/%s" % slug)

		if os.path.isfile("/etc/nginx/sites-available/%s" % slug):
			print "Removing virtualhost config"
			os.unlink("/etc/nginx/sites-available/%s" % slug)
		return

	if not os.path.isdir(vhost_path):
		print "Creating vhost directory: %s" % vhost_path
		args = (wp_cli, vhost_path, wordpress_version)
		cmd = "%s core download --path=%s --version=%s --allow-root" % args
		print cmd
		out, err, code = run_command(cmd)
		if code != 0:
			error_out(err)

	if not os.path.isfile(os.path.join(vhost_path, "wp-config.php")):
		print "Creating config file: %s" % os.path.join(vhost_path, "wp-config.php")
		args = (wp_cli, vhost_path, db_name, db_host, db_user, db_pass)
		cmd = "%s core config --allow-root --path=%s --dbname=%s --dbhost=%s --dbuser=%s --dbpass=%s" % args
		print cmd
		out, err, code = run_command(cmd)
		if code != 0:
			error_out(err)
		f = open(os.path.join(vhost_path, 'wp-config.php'), "r")
		content = f.read()
		f.close()
		content = content.replace("$table_prefix = 'wp_'", "$table_prefix = '%s_'" % slug)
		f = open(os.path.join(vhost_path, 'wp-config.php'), "w+")
		f.write(content)
		f.close()

	out, err, code = run_command("%s core is-installed --allow-root --path=%s" % (wp_cli, vhost_path))
	if code != 0:
		print "Installing Wordpress multisite"
		args = (wp_cli, vhost_path, staging_host, site_title, admin_user, admin_password, email)
		cmd = "%s core multisite-install --allow-root --path=%s --url=%s --title='%s' --admin_user=%s --admin_password='%s' --admin_email=%s" % args
		print cmd
		out, err, code = run_command(cmd)
		if code != 0:
			error_out(err)

	if staging_tools:
		print "Activating cloning plugin"
		args = (os.path.join(repos_root, configs_dir), vhost_path)
		cmd = "cp -r %s/plugins/production-cloner %s/wp-content/plugins/" % args
		out, err, code = run_command(cmd)
		if code != 0:
			error_out(err)

		args = (wp_cli, vhost_path)
		cmd = "%s plugin activate production-cloner --path=%s --allow-root --network" % args
		out, err, code = run_command(cmd)
		if code != 0:
			error_out(err)

	print "Updating vhost config"
	f = open(os.path.join(repos_root, configs_dir, "templates", "vhost.conf"), "r")
	content = f.read()
	f.close()
	content = content.replace("{hostname}", staging_host)
	content = content.replace("{slug}", slug)

	print "Enabling vhost"
	f = open("/etc/nginx/sites-available/%s" % slug, "w+")
	f.write(content)
	f.close()

	if not os.path.islink("/etc/nginx/sites-enabled/%s"  % slug):
		os.symlink("/etc/nginx/sites-available/%s" % slug, "/etc/nginx/sites-enabled/%s" % slug)

repos_root = "/usr/local/share/rsmm/sites"
configs_dir = "site_configs"
themes_dir = "themes/files"
vhosts_dir = "/var/www/vhosts"

site_title = "RSMM Staging"
git_branch = "{{ repo_branch }}"
db_name = "{{ db_name }}"
db_host = "{{ db_host }}"
db_user = "{{ db_user }}"
db_pass = "{{ db_pass }}"
admin_user = "{{ wp_admin_user }}"
admin_password = "{{ wp_admin_pass }}"
email = "{{ wp_admin_email }}"
staging_domain = "{{ wp_hostname }}"
wordpress_version = "{{ wordpress_version }}"
multisite_enabled = {{ multisite|default(False) }}
staging_tools = {{ enable_staging_tools|default(False) }}

if not os.path.isdir(repos_root):
	error_out("Error. Cannot find %s" % repos_root)

if not os.path.isdir(os.path.join(repos_root, configs_dir)):
	error_out("Error. Cannot find %s" % os.path.join(repos_root, configs_dir))

if not os.path.isdir(os.path.join(repos_root, themes_dir)):
	error_out("Error. Cannot find %s" % os.path.join(repos_root, themes_dir))

if not os.path.isdir(vhosts_dir):
	error_out("Error. Cannot find %s" % vhosts_dir)

out, err, code = run_command("which wp")
if code != 0:
	error_out("Error. Could not find where wp-cli was installed.")
wp_cli = out.split("\n")[0]

cmd = "cd %s && git pull origin %s" % (os.path.join(repos_root, configs_dir), git_branch)
print "Updating repository: %s" % cmd
out, err, code = run_command(cmd)

if code != 0:
	error_out(err, code)
print out

if not os.path.isdir(os.path.join(repos_root, configs_dir, "sites")):
	error_out("Error. Cannot find %s" % os.path.join(repos_root, configs_dir, "sites"))

if not os.path.isdir(os.path.join(repos_root, configs_dir, "templates")):
	error_out("Error. Cannot find %s" % os.path.join(repos_root, configs_dir, "templates"))

if not os.path.isfile(os.path.join(repos_root, configs_dir, "templates", "vhost.conf")):
	error_out("Error. Cannot find %s" % os.path.join(repos_root, configs_dir, "templates", "vhost.conf"))

if not os.path.isfile(os.path.join(repos_root, configs_dir, "config.yml")):
	error_out("Error. Cannot find %s" % os.path.join(repos_root, configs_dir, "config.yml"))

f = open(os.path.join(repos_root, configs_dir, "config.yml"), "r")
global_config = yaml.load(f.read())
f.close()

required_params = ["production_db_name", "production_db_user", "production_db_pass", "production_db_host"]

for param_name in required_params:
	if not global_config.has_key(param_name):
		error_out("Error. %s is not defined in %s." % (param_name, os.path.join(repos_root, configs_dir, "config.yml")))

for fname in os.listdir(os.path.join(repos_root, configs_dir, "sites")):
	if os.path.isfile(os.path.join(repos_root, configs_dir, "sites", fname, "config.yml")):
		f = open(os.path.join(repos_root, configs_dir, "sites", fname, "config.yml"))
		config = yaml.load(f.read())
		f.close()
		if multisite_enabled:
			multisite(config)
		else:
			singlesite(config)

print "Restarting Nginx"
out, err, code = run_command("service nginx restart")
print out