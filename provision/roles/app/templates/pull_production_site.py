#!/usr/bin/python

import os, os.path
import sys
from subprocess import Popen, PIPE
import yaml

def error_out(msg="", exit_code=1):
	stdout = sys.stderr
	print msg
	sys.exit(1)

def run_command(cmd, data=None):
	result = Popen(cmd, stdout=PIPE, stderr=PIPE, shell=True)
	out = result.stdout.read()
	err = result.stderr.read()
	result.communicate(data)
	code = result.returncode
	return [out, err, code]

if len(sys.argv) < 2:
	error_out("Error. Slug not provided")

slug = sys.argv[1]

repos_root = "/usr/local/share/rsmm/sites"
configs_dir = "site_configs"
themes_dir = "themes/files"
vhosts_dir = "/var/www/vhosts"
snapshot_dir = "/tmp/rsmm"

git_branch = "{{ repo_branch }}"
db_name = "{{ db_name }}"
db_host = "{{ db_host }}"
db_user = "{{ db_user }}"
db_pass = "{{ db_pass }}"

if not os.path.isdir(repos_root):
	error_out("Error. Cannot find %s" % repos_root)

if not os.path.isdir(os.path.join(repos_root, configs_dir)):
	error_out("Error. Cannot find %s" % os.path.join(repos_root, configs_dir))

if not os.path.isdir(os.path.join(repos_root, themes_dir)):
	error_out("Error. Cannot find %s" % os.path.join(repos_root, themes_dir))

if not os.path.isdir(vhosts_dir):
	error_out("Error. Cannot find %s" % vhosts_dir)

out, err, code = run_command("which wp")
if code != 0:
	error_out("Error. Could not find where wp-cli was installed.")
wp_cli = out.split("\n")[0]

#cmd = "cd %s && git pull origin %s" % (os.path.join(repos_root, configs_dir), git_branch)
#print "Updating repository: %s" % cmd
#out, err, code = run_command(cmd)

#if code != 0:
#	error_out(err, code)
#print out

if not os.path.isdir(os.path.join(repos_root, configs_dir, "sites")):
	error_out("Error. Cannot find %s" % os.path.join(repos_root, configs_dir, "sites"))

if not os.path.isdir(os.path.join(repos_root, configs_dir, "templates")):
	error_out("Error. Cannot find %s" % os.path.join(repos_root, configs_dir, "templates"))

if not os.path.isfile(os.path.join(repos_root, configs_dir, "templates", "vhost.conf")):
	error_out("Error. Cannot find %s" % os.path.join(repos_root, configs_dir, "templates", "vhost.conf"))

if not os.path.isfile(os.path.join(repos_root, configs_dir, "config.yml")):
	error_out("Error. Cannot find %s" % os.path.join(repos_root, configs_dir, "config.yml"))

f = open(os.path.join(repos_root, configs_dir, "config.yml"), "r")
global_config = yaml.load(f.read())
f.close()

required_params = ["production_db_name", "production_db_user", "production_db_pass", "production_db_host"]

for param_name in required_params:
	if not global_config.has_key(param_name):
		error_out("Error. %s is not defined in %s." % (param_name, os.path.join(repos_root, configs_dir, "config.yml")))

if not os.path.isdir(snapshot_dir):
	os.mkdir(snapshot_dir)

args = (global_config['production_db_name'], global_config['production_db_user'], global_config['production_db_pass'], global_config['production_db_host'], slug)
cmd = "mysql %s -u %s -p'%s' -h %s -e \"select blog_id, path from wp_blogs\" | grep %s | grep -Eo '[0-9]*'" % args
print cmd
out, err, code = run_command(cmd)
if code != 0:
	error_out(err, code)
blog_id = out.split("\n")[0]

dump_tables = ["wp_users", "wp_%s_commentmeta" % blog_id, \
"wp_%s_comments" % blog_id, \
"wp_%s_links" % blog_id, \
"wp_%s_options" % blog_id, \
"wp_%s_postmeta" % blog_id, \
"wp_%s_posts" % blog_id, \
"wp_%s_term_relationships" % blog_id, \
"wp_%s_term_taxonomy" % blog_id, \
"wp_%s_terms" % blog_id]

snapshot_name = "%s/snapshot-%s.sql" % (snapshot_dir, os.getpid())

args = (global_config['production_db_name'], global_config['production_db_user'], global_config['production_db_host'], global_config['production_db_pass'], " ".join(dump_tables), blog_id, slug, snapshot_name)
cmd = "mysqldump %s -u %s -h %s -p'%s' --tables %s | sed s/wp_%s_/%s_/g > %s" % args
print "Creating Snapshot"
print cmd
out, err, code = run_command(cmd)
if code != 0:
	error_out(err, code)

args = (db_name, db_user, db_pass, db_host, snapshot_name)
cmd = "mysql %s -u %s -p'%s' -h %s < %s" % args
print "Loading snapshot"
print cmd
out, err, code = run_command(cmd)
if code != 0:
	error_out(err, code)

print "Deleting snapshot"
os.unlink(snapshot_name)

print "Rewriting Urls"

args = (global_config['production_db_name'], global_config['production_db_user'], global_config['production_db_pass'], global_config['production_db_host'], blog_id)
cmd = "mysql %s -u %s -p'%s' -h %s -e \"select blog_id, domain from wp_blogs\" | grep %s | grep -Eo '[_a-zA-Z\.-]+'" % args
out, err, code = run_command(cmd)
if code != 0:
	error_out(err, code)
production_string = out.split("\n")[0]

args = (db_name, db_user, db_pass, db_host, slug)
cmd = "mysql %s -u %s -p'%s' -h %s -e \"select blog_id, domain from %s_blogs\" | grep 1 | grep -Eo '[_a-zA-Z\.-]+'" % args
out, err, code = run_command(cmd)
if code != 0:
	error_out(err, code)
staging_string = out.split("\n")[0]

vhost_path = "%s/%s" % (vhosts_dir, slug)

args = (wp_cli, production_string, staging_string, vhost_path)
cmd = "%s search-replace '%s' '%s' --path=%s --allow-root --network" % args
out, err, code = run_command(cmd)
if code != 0:
	error_out(err, code)

print "Activating cloning plugin"
args = (os.path.join(repos_root, configs_dir), vhost_path)
cmd = "cp -r %s/plugins/production-cloner %s/wp-content/plugins/" % args
out, err, code = run_command(cmd)
if code != 0:
	error_out(err)

args = (wp_cli, vhost_path)
cmd = "%s plugin activate production-cloner --path=%s --allow-root --network" % args
out, err, code = run_command(cmd)
if code != 0:
	error_out(err)

print "Done!"